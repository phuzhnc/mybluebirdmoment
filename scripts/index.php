<?php 

require('/home/fuzzincc/www/mybluebird/IncludeLibraries.php');

$facebook = new Facebook(array(
  'appId'  => APP_ID,
  'secret' => APP_SECRET,
));

$user = $facebook->getUser();
if ($user == null || $user == 0 || $user == false) {
	header('Location: http://fuzzinc.com/mybluebird/lib/facebookPHPSDK/src/login.php');
} 
$databaseManager = new UserDatabaseManager();
try {
    // Proceed knowing you have a logged in user who's authenticated.
    $user_profile = $facebook->api('/me');
  } catch (FacebookApiException $e) {
    error_log($e);
    echo "We have an error establishing a connection to your facebook account";
}
 
$databaseManager->addUser($user,$facebook->getExtendedAccessToken(),$user_profile['first_name'], $user_profile['last_name']);
?>
<html>
<head><title>My Bluebird</title>
<style>
	div#backgroundBanner {
		background-image:url('pictures/backgroundImage.png'); background-repeat:no-repeat; width:800px; height:400px;
		margin-left:auto; margin-right:auto; 
	}
</style>
</head>
<body>
	<div id="backgroundBanner" >
		<div id="dailyBibleVerse">

		</div>
	</div>
</body>
</html>
