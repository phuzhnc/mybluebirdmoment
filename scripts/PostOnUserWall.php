<?php
require('/home/fuzzincc/www/mybluebird/IncludeLibraries.php');

$facebook = new Facebook(array('appId' => APP_ID, 'secret' => APP_SECRET,'cookie' => true,));
$userDatabaseManager = new UserDatabaseManager();
$bibleDatabaseManager = new BibleDatabaseManager();

$userAccessTokenArray = $userDatabaseManager->getUserAccessTokenArray();
shuffle($userAccessTokenArray);
$userIndex1 = rand()%count($userAccessTokenArray);
$userIndex2 = rand()%count($userAccessTokenArray);
$count = 0;

function gauss1($algorithm = "polar") {
	$randmax = 9999;

	switch($algorithm) {

		//polar-methode by marsaglia
		case "polar":
			$v = 2;
			while ($v > 1) {
				$u1 = rand(0, $randmax) / $randmax;
				$u2 = rand(0, $randmax) / $randmax;

				$v = (2 * $u1 - 1) * (2 * $u1 - 1) + (2 * $u2 - 1) * (2 * $u2 - 1);
			}

			return (2* $u1 - 1) * (( -2 * log($v) / $v) ^ 0.5);

		// box-muller-method
		case "boxmuller":
			do {
				$u1 = rand(0, $randmax) / $randmax;
				$u2 = rand(0, $randmax) / $randmax;                   

				$x = sqrt(-2 * log($u1)) * cos(2 * pi() * $u2);
			} while (strval($x) == "1.#INF" or strval($x) == "-1.#INF");

			// the check has to be done cause sometimes (1:10000)
			// values such as "1.#INF" occur and i dont know why

			return $x;

		// twelve random numbers  
		case "zwoelfer":
			$sum = 0;
			for ($i = 0; $i < 12; $i++) {
				$sum += rand(0, $randmax) / $randmax;
			}
			return $sum;
	}      
} 

function gauss()
{   // N(0,1)
	    // returns random number with normal distribution:
	    //   mean=0
	    //   std dev=1
	   
	    // auxilary vars
	    $x=random_0_1();
	        $y=random_0_1();
		   
		    // two independent variables with normal distribution N(0,1)
		    $u=sqrt(-2*log($x))*cos(2*pi()*$y);
		        $v=sqrt(-2*log($x))*sin(2*pi()*$y);
			   
			    // i will return only one, couse only one needed
			    return $u;
}

function gauss_ms($m=0.0,$s=1.0)
{   // N(m,s)
	    // returns random number with normal distribution:
	    //   mean=m
	    //   std dev=s
	   
	    return gauss()*$s+$m;
}

function random_0_1()
{   // auxiliary function
	    // returns random number with flat distribution from 0 to 1
	    return (float)rand()/(float)getrandmax();
}

foreach($userAccessTokenArray as $userID => $accessToken) {
	$facebook->setAccessToken($accessToken);
	$privacy = array( 'value' => 'ALL_FRIENDS' );
	$bibleverse = $bibleDatabaseManager->getRandomBibleVerse();
	$attachment = array(
		'privacy' => json_encode($privacy), 
		'name' => 'My Bluebird',
 		#'caption' => $bibleverse, 
		'description' => $bibleverse, 
		'link' => 'mybluebirdmoment.com',
		'picture' => 'http://mybluebirdmoment.com/home/wp-content/uploads/2012/05/mybluebirdbanner22.png',
	);
//	if($count == $userIndex1 || $count == $userIndex2) {
	//if($count == $userIndex1 ) {
	$normalNumber = gauss();
	print($normalNumber."\n");
	if($normalNumber > 1.2) {
	//if($userID == 6018698 ) {
	//if($userID == 521004199) {
		try {
		//	echo $bibleverse;
			echo("Post: $userID\n");
			$result = $facebook->api("/me/feed", 'post', $attachment);
		}
		catch(FacebookApiException $e) {
			error_log($e);	
			$userDatabaseManager->removeUser($userID);
		}
	}
	$count++;
}
?>
