<?php
/**
 * Header Template
 *
 *
 * @file           header.php
 * @package        Responsive 
 * @author         Emil Uzelac 
 * @copyright      2003 - 2012 ThemeID
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/header.php
 * @link           http://codex.wordpress.org/Theme_Development#Document_Head_.28header.php.29
 * @since          available since Release 1.0
 */
require('/home/fuzzincc/www/mybluebird/IncludeLibraries.php');
$facebook = new Facebook(array('appId'  => APP_ID,'secret' => APP_SECRET,'cookie' => true,));

$_POST['facebook_login_user_subscribed'] = false;
$databaseManager = new UserDatabaseManager();
$user = $facebook->getUser();
$_POST['userid'] = $user;
//echo $user;
if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) {

	$protocolType = 'https';
}
else {
	$protocolType = 'http';
}

$url  = $protocolType.'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$parseURL = parse_url($url);
$url = $protocolType.'://'.$parseURL['host'].$parseURL['path'];
//echo $url;
if( strpos($url, 'facebook') == true || strpos($url, 'stablehost') || isset($_GET['isfacebook']) ) {
	$url = $protocolType.'://apps.facebook.com/mybluebird';
}

$_POST['redirect'] = $url;
$_POST['facebook_login_user_url'] = 'http://mybluebirdmoment.com/lib/facebookPHPSDK/src/login.php?redirect='.$url;  
if ($user == null || $user == 0 || $user == false) {
	$facebook->destroySession();
	$_POST['facebook_login_user_subscribed'] = false;
} 
else {
	try {
		if($databaseManager->isUserInDatabase($user)) {
			$_POST['is_facebook_user_subscribed'] = true;
			$databaseManager->addUser($user,$facebook->getExtendedAccessToken(),$user_profile['first_name'], $user_profile['last_name']);
		}
		else {
			$user_profile = $facebook->api('/me');
			$permissions = $facebook->api("/me/permissions");
			if (array_key_exists('publish_stream', $permissions['data'][0])  ) {
				$_POST['is_facebook_user_subscribed'] = true;
				$databaseManager->addUser($user,$facebook->getExtendedAccessToken(),$user_profile['first_name'], $user_profile['last_name']);
			} else{
				$_POST['is_facebook_user_subscribed'] = false;
			}
		}
	} catch (FacebookApiException $e) {
		//echo $e;
		error_log("$e");

	}
}

?>
<!doctype html>
<!--[if lt IE 7 ]> <html class="no-js ie6" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	
<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title><?php wp_title('&#124;', true, 'right'); ?><?php bloginfo('name'); ?></title>

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<link href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic,700italic" rel="stylesheet" type="text/css">
<?php wp_enqueue_style('responsive-style', get_stylesheet_uri(), false, '1.2.1');?>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
                 
<?php responsive_container(); // before container hook ?>
<div id="container" class="hfeed">
         
    <?php responsive_header(); // before header hook ?>
    <div id="header">
    
        <?php if (has_nav_menu('top-menu', 'responsive')) { ?>
	        <?php wp_nav_menu(array(
				    'container'       => '',
					'menu_class'      => 'top-menu',
					'theme_location'  => 'top-menu')
					); 
				?>
        <?php } ?>
        
    <?php responsive_in_header(); // header hook ?>
   
	<?php if ( get_header_image() != '' ) : ?>
               
        <div id="logo">
            <a href="<?php echo home_url('/'); ?>"><img src="<?php header_image(); ?>" width="<?php if(function_exists('get_custom_header')) { echo get_custom_header() -> width;} else { echo HEADER_IMAGE_WIDTH;} ?>" height="<?php if(function_exists('get_custom_header')) { echo get_custom_header() -> height;} else { echo HEADER_IMAGE_HEIGHT;} ?>" alt="<?php bloginfo('name'); ?>" /></a>
        </div><!-- end of #logo -->
        
    <?php endif; // header image was removed ?>

    <?php if ( !get_header_image() ) : ?>
                
        <div id="logo">
            <span class="site-name"><a href="<?php echo home_url('/'); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a></span>
            <span class="site-description"><?php bloginfo('description'); ?></span>
        </div><!-- end of #logo -->  

    <?php endif; // header image was removed (again) ?>
			    
				<?php wp_nav_menu(array(
				    'container'       => '',
					'theme_location'  => 'header-menu')
					); 
				?>
                
            <?php if (has_nav_menu('sub-header-menu', 'responsive')) { ?>
	            <?php wp_nav_menu(array(
				    'container'       => '',
					'menu_class'      => 'sub-header-menu',
					'theme_location'  => 'sub-header-menu')
					); 
				?>
            <?php } ?>
 
    </div><!-- end of #header -->
    <?php responsive_header_end(); // after header hook ?>
    
	<?php responsive_wrapper(); // before wrapper ?>
    <div id="wrapper" class="clearfix">
    <?php responsive_in_wrapper(); // wrapper hook ?>
