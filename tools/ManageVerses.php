<?php

require('/home/fuzzincc/www/mybluebird/IncludeLibraries.php');
$approvedUsers = Array(
		'6018698', //phong
		'1062930393', //sam chan		
		'521004199', //jacob
		'1034760463', //emanual
		);

#$facebook = new Facebook(array(
#  'appId'  => APP_ID,
#  'secret' => APP_SECRET,
#));

#$user = $facebook->getUser();
#if ($user == null || $user == 0 || $user == false) {
#	header('Location: http://mybluebirdmoment.fuzzinc.com/mybluebird/lib/facebookPHPSDK/src/login.php?redirect=http://mybluebirdmoment.fuzzinc.com/mybluebird/tools/ManageVerses.php');
#}
#if( !in_array($user, $approvedUsers) ) {
#	echo 'You do not have permission to manage the bible database. Please contact the webadmin to gain access';
#	$facebook->destroySession();
#	exit;
#}

global $bookListArray;

$bibleDatabaseManager = new BibleDatabaseManager();
if(isset($_GET) && count($_GET) > 0) {
	if($_GET['delete'] == true) {
		$bibleDatabaseManager->deleteBibleVerseID($_GET['primaryKey']);
	}
}
if(isset($_POST) && count($_POST) > 0) {
	if(!isset($_POST['book']) || $_POST['book'] == "" ) {
		echo "<p style=\"color:red\">Error: Must define a book</p>";
	}
	else {
		$bibleDatabaseManager->insertBibleVerseFromHTMLPost($_POST);
		unset($_POST);
	}
}
?>

<html>
	<head>
	<style>
		table.db-table     { border-right:1px solid #ccc; border-bottom:1px solid #ccc; }
		table.db-table th  { background:#eee; padding:5px; border-left:1px solid #ccc; border-top:1px solid #ccc; }
		table.db-table td  { padding:5px; border-left:1px solid #ccc; border-top:1px solid #ccc; }

		label {padding-right:5px; padding-left:5px }
		input[type=text] {width:50px; }
		input[type=submit] {margin-left:20px  }
	</style>
	</head>
	<body>

	<form method="post" action="http://fuzzinc.com/mybluebird/tools/ManageVerses.php" >
		<label>Book:</label>
		<select name="book">
			<option value="">Select...</option>
			<?php 
				foreach($bookListArray as $element) {
					echo '<option value="'.$element.'">'.$element.'</option>';
				}		
			?>
		</select>
		<label>Chapter:</label><input type="text" name="chapter" />
		<label>Start Verse:</label><input type="text" name="startverse" />
		<label>End Verse:</label><input type="text" name="endverse" />
		<input type="submit" value="Add To Database" />
	</form>
<?php
$bibleDatabaseManager->generateTableHTML("BibleVerses");
?>
	</body>
</html>
