<?php

require_once('DatabaseManager.php');

class BibleDatabaseManager extends DatabaseManager {
	public function insertBibleVerseFromHTMLPost($post) { 
		if($post['endverse'] == null || !isset($post['endverse'])) {
			$post['endverse'] = $post['startverse'];
		}
		else {
			if($post['endverse'] < $post['startverse']) {
				echo "<p style=\"color:red\">Error: endverse must be greater than start verse</p>";
				return false;
			}
		}
		$verse = $this->generateBibleVerse($post['book'], $post['chapter'], $post['startverse'], $post['endverse'], "WEB");
		if(strlen($verse) > 0) {
			//echo "Bible: ".$verse;
			$verseSQL = mysql_real_escape_string($verse);
			$queryString = 'INSERT INTO BibleVerses (VerseID, Book, Chapter, StartVerse, EndVerse, Version, Verse) VALUES(null,\''.$post['book'].'\', '.$post['chapter'].', '.$post['startverse'].', '.$post['endverse'].', '.'\'WEB\',\''."$verseSQL".'\')'; 
			//echo $queryString;				
			mysql_query($queryString);
				//echo mysql_errno() . ": " . mysql_error(). "\n";
		}
		else {
			echo "<p style=\"color:red\">Error: Unable to find the Bible verse</p>";
			return false;
		}
		echo '<h3>Successfully added bible verse to database</h3>';
		echo '<table cellpadding="0" cellspacing="0" class="db-table">';
		echo '<tr><td>VerseID</td><td>Book</td><td>Chapter</td><td>StartVerse</td><td>EndVerse</td><td>Version</td><td>Verse</td></tr>';
		echo '<tr><td><input type="button" value="delete" OnClick="location.href=\'http://fuzzinc.com/mybluebird/tools/ManageVerses.php?delete=true&primaryKey='.mysql_insert_id().'\'"/></td><td>'.$post['book'].'</td><td>'.$post['chapter'].'</td><td>'.$post['startverse'].'</td><td>'.$post['endverse'].'</td><td>'.'WEB'.'</td><td>'.$verse.'</td></tr>';
		echo '</table><br />';
		return true;
	}
	public function deleteBibleVerseID($verseID) {
		mysql_query('DELETE FROM BibleVerses WHERE VerseID='.$verseID);
	}
	public function getRandomBibleVerse() {
		$randomNumber = rand();		
		$results = mysql_query('SELECT * FROM BibleVerses'); 
		if(!$results) {
			die("Error: mysql_error()");
		}
		$numberOfRows = mysql_num_rows($results);
		$selectIndex = $randomNumber%$numberOfRows;
		$count = 0;
		while($row = mysql_fetch_assoc($results)) {
			if($count == $selectIndex) {
				if( $row['StartVerse'] == $row['EndVerse'] ) {
					return '"'.trim($row['Verse'], '"').'"'.' -- '.$row['Book'].' '.$row['Chapter'].':'.$row['StartVerse'];
				}
				else {
					return '"'.trim($row['Verse'], '"').'"'.' -- '.$row['Book'].' '.$row['Chapter'].':'.$row['StartVerse'].'-'.$row['EndVerse'];
				}
			}
			$count++;
		}	
	}
	public function generateBibleVerse($book, $chapter, $startverse, $endverse) {
		$returnString = "";
		$results = mysql_query('SELECT id FROM BibleBooks WHERE name=\''.$book.'\'');
		if(!$results) {
			die('Error1: '.mysql_error());
		}
		$row = mysql_fetch_assoc($results);
		$bookIndex = $row['id'];
		for($i = $startverse; $i <= $endverse; $i++) {
			$queryString = 'SELECT t FROM BibleWEB WHERE b='.$bookIndex.' AND c='.$chapter.' AND v='.$i;
			$results = mysql_query($queryString);	
			if(!$results) {
				die('Error2: '.mysql_error());
			}
			$row = mysql_fetch_assoc($results);	
			$formatedString = preg_replace('/&[^;]*;/','', htmlspecialchars_decode( strip_tags($row['t']) ) );
			$returnString = $returnString.$formatedString.' ';
		}
		return trim($returnString);
	}
}
?>
