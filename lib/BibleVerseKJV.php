<?php
require('simple_html_dom.php');

class BibleVerseKJV {
	public $www = "http://beta.biblegateway.com/api/1/bible/";
	public function BibleVerse() {
	}
	public function queryBibleVerse($queryString) {
		$html = file_get_html($queryString);
		$result_string = json_decode($html);
		return $result_string->data[0]->text;
	}
	public function generateBibleVerse($book, $chapter, $startVerse, $endVerse, $version) {
		return  $this->queryBibleVerse($this->generateBibleVerseQueryString($book, $chapter, $startVerse, $endVerse, $version));
	}
	public function generateBibleVerseQueryString($book, $chapter, $startVerse, $endVerse, $version) {
		$correctBookFormat = $this->getCorrectBookFormat($book);
		$return_string = $this->www.$correctBookFormat.'.'.$chapter.'.'.$startVerse.'-'.$correctBookFormat.'.'.$chapter.'.'.$endVerse.'/'.$version;
		return $return_string;
	}
	public function getCorrectBookFormat($book) {
		$splitBook = explode(" ", $book);
		if(count($splitBook) == 1) {
			return $book;
		}
		else if ( $book == 'Song of Songs' ){
				return 'songofsongs';
		}
		else {
			if(count($splitBook) == 2) {
				return $splitBook[0].$splitBook[1];
			}	
			else {
				echo "error!";
				return false;
			}
		}
	}
}
?>
