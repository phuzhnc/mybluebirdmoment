<?php

require_once('DatabaseManager.php');

class UserDatabaseManager extends DatabaseManager {
	public function isUserInDatabase($userID) {
		$result = mysql_query('SELECT UserID FROM Users WHERE UserID=\''.$userID.'\'');
		$size = mysql_num_rows($result);
		if($size == 0) {
			return false;
		}
		else {
			return true;
		}
	}
	public function addUser($userID, $accessToken, $firstname, $lastname) {
		if($userID == 0) {
			die( "Error: Cannot insert a user with id: 0");
			return;
		}
		if($this->isUserInDatabase($userID)) {
			$this->insertNewAccessToken($userID,$accessToken);
		}			
		else {
			$this->insertNewUser($userID, $accessToken, $firstname, $lastname);
		}
	}
	public function insertNewUser($userID, $accessToken, $firstname, $lastname) {
		if(!mysql_query('INSERT INTO Users (UserID, AccessToken, FirstName, LastName) VALUES( \''.$userID.'\', \''.$accessToken.'\', \''.$firstname.'\', \''.$lastname.'\')')) {
			die('Error: ' . mysql_error());
		}
	}
	public function insertNewAccessToken($userID, $accessToken) {
		if(!mysql_query('UPDATE Users SET AccessToken=\''.$accessToken .'\' WHERE UserID=\''. $userID.'\'')) {
			die('Error: ' . mysql_error());
		}
	}
	public function removeUser($userID) {
		if(!mysql_query('DELETE FROM Users WHERE UserID=\''.$userID.'\'')) {
			return false;
			die('Error: ' . mysql_error());
		}
		return true;
	}
	public function getUserAccessTokenArray() {
		$userAccessTokenArray = array();
		$result = mysql_query('SELECT UserID,AccessToken FROM Users');
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
		    $userAccessTokenArray[$row['UserID']] = $row['AccessToken'];
		}
		return $userAccessTokenArray;
	}
}
?>
